const EC = protractor.ExpectedConditions;

class BasePage {

  get(url) {
    browser.get(url);
  }

  waitForElement(elm) {
    browser.wait(EC.presenceOf(elm), 5000);
    browser.wait(EC.stalenessOf(elm), 5000);
  }

}

module.exports = BasePage;
