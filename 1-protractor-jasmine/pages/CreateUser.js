const BasePage = require('./BasePage');

class CreateUser extends BasePage {

  constructor() {
    super();
    this.createUserBtn = $('button[ng-click="pop()"]');
    this.firstNameInput = $('input[name="FirstName"]')
  }

  createUser() {
    this.createUserBtn.click();
    this.firstNameInput.sendKeys('testUser');
    $('input[name="LastName"]').sendKeys('testLastName');
    $('input[name="UserName"]').sendKeys('testUserName');
    $('input[name="Password"]').sendKeys('testpass');
    $('input[name="optionsRadios"][value="15"]').click();
    $('select[name="RoleId"]').sendKeys('Customer');
    $('input[name="Email"]').sendKeys('q@qqq.com');
    $('input[name="Mobilephone"]').sendKeys('33321312312');
    $('.btn-success').click();
    browser.sleep(2000);
  }

  getTextBtn() {
    this.createUserBtn.getText().then(text => console.log(text));
  }
}

module.exports = CreateUser;
