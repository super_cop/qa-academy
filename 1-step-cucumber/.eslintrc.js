module.exports = {
  "extends": ["eslint-config-airbnb-base"],
  "rules": {
    'max-len': ["error", 120],
    'strict': 'off',
    'class-methods-use-this': 'off',
    'new-cap': ['error', {
      "capIsNew": false
    }],
  },
  "env": {
    "browser": true,
    "node": true,
    "protractor": true
  },
  "plugins": [
    "protractor"
  ]
};
