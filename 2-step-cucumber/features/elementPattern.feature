Feature: Element pattern

  Scenario: Element pattern
    Given I open home page
    Then I can verify "http://www.way2automation.com/angularjs-protractor/webtables/" url
    Then I can see row with data
    |First name |
    | test      |
